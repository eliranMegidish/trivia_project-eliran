#pragma once

#include <vector>
#include <list>
#include "LoggedUser.h"

using std::vector;
using std::list;


typedef struct RoomData
{ 
	// struct that contian the information about room
	unsigned int _id;
	string _name;
	int _maxPlayers;
	unsigned int _timePerQuestion;
	unsigned int _numberOfQuestion;
	unsigned int _isActive;// (1)-can join to the room , (2)-room in playing , (0)-room close canot join the room .

}RoomData;

class Room
{

private:
	vector<LoggedUser>  m_users; // vector that conatin players in this room // first i insert the user that create the room.
	RoomData m_metadata;// information about the room

public:
	RoomData getStructRoomData();

	void setRoomData(unsigned int id,string nameRoom, int maxPlayers, unsigned int timePerQuestion, unsigned int numberOfQuestion,unsigned int isActive);
	void setId(unsigned int id);
	void setIsActive(unsigned int flag);

	bool addUser(LoggedUser user);
	void removeUser(LoggedUser user);
	vector<string> getAllUsers();
};




