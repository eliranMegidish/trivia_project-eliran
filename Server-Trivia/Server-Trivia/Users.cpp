#include "User.h"

User::User()
{
	this->name = "";
	this->password = "";
	this->email = "";
}

void User::setName(string name)
{
	this->name = name;
}

void User::setPassword(string password)
{
	this->password = password;
}

void User::setEmail(string email)
{
	this->email = email;
}

string User::getName()
{
	return this->name;
}

string User::getPassword()
{
	return this->password;
}

string User::getEmail()
{
	return this->email;  
}

std::ostream& operator<<(std::ostream& os, const User& user)
{
	os << "name:"<<user.name << " password:" << user.password << " email:" << user.email;
	return os;
}
