#pragma once
#include "iostream"
#include "sqlite3.h"
#include <list>
#include <vector>


using std::string;
using std::endl;
using std::cout;

class IDatabase
{
public:

	virtual bool open() = 0;
	virtual bool doesUserExist(string userName)=0;
	virtual bool doesPasswordMatch(string name,string password)=0;
	virtual bool addNewUser(string userName, string password, string email)=0;
	//List<Question> getQuestions(int)
	virtual float getPlayerAverageAnswerTime(string name) = 0;
	virtual int getNumOfCorrectAnswers(string name) = 0;
	virtual int getNumOfTotalAnswers(string name) = 0;
	virtual int getNumOfPlayerGames(string name) = 0;
	virtual std::vector <string> getAllUserName() = 0;


};