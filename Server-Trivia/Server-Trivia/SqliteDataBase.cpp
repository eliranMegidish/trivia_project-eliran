#include "SqliteDataBase.h"

bool SqliteDataBase::open()
{
	int res = sqlite3_open(this->_dbFileName.c_str(), &this->_db);
	if (res != SQLITE_OK)
	{
		this->_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}
	return true;
}


bool SqliteDataBase::sendData(const char* query, int(*callback)(void*, int, char**, char**))
{
	std::cout << query << std::endl;
	int res = sqlite3_exec(this->_db, query, callback, nullptr, nullptr);
	return res;
}

int SqliteDataBase::callbackUsers(void* data, int argc, char** argv, char** azColName)
{
	User user;
	for (int i = 0; i < argc; i++)//����� �� ����� ������� 
	{
		if (std::string(azColName[i]) == "NAME")
		{
			user.setName(std::string(argv[i]));
		}
		else if (std::string(azColName[i]) == "USER_PASSWORD")
		{
			user.setPassword(std::string(argv[i]));
		}
		else if (std::string(azColName[i]) == "EMAIL")
		{
			user.setEmail(std::string(argv[i]));
		}
	}
	((std::list<User>*)data)->push_back(user);
	return 0;
}

int SqliteDataBase::callbackUserStatus(void* data, int argc, char** argv, char** azColName)
{
	for (int i = 0; i < argc; i++)//����� �� ����� ������� 
	{
		if (std::string(azColName[i]) == "AvgTimeAns")
		{  
			if (argv[i] != NULL)
			{
				((User*)data)->status.setAverageAnswerTime(atof(argv[i]));
			}
			else
			{
				((User*)data)->status.setAverageAnswerTime(0);

			}
		}
		else if (std::string(azColName[i]) == "NumAns")
		{
			cout << "NumAns:" << (argv[i]) << endl;
			((User*)data)->status.setTotalAnswers(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NumOfGmae")
		{
			((User*)data)->status.setPlayerGames(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NumCorrectAns")
		{
			((User*)data)->status.setCorrectAnswers(atoi(argv[i]));
		}
	}
	return 0;
}

int SqliteDataBase::callbackGetAllUsers(void* data, int argc, char** argv, char** azColName)
{
	for (int i = 0; i < argc; i++)//����� �� ����� ������� 
	{
		if (std::string(azColName[i]) == "NAME")
		{
			cout << argv[i] << endl;
			((std::vector<string>*)data)->push_back(argv[i]);
		}
	}
	return 0;
}


/*
init the all user that in the game 
*/
void SqliteDataBase::initUsers()
{
	this->_ListUsers.clear();
	this->_query = "SELECT * FROM USERS ;";
	int res = sqlite3_exec(this->_db, _query.c_str(), SqliteDataBase::callbackUsers, &(this->_ListUsers), nullptr);
	if (res != SQLITE_OK)
	{
		cout << "Error query" << endl;
	}
}



void SqliteDataBase::printUsers()
{
	this->initUsers();//init the list of users
	for (auto& user : this->_ListUsers)
	{
		cout << user << endl;
	}
}

std::vector<string> SqliteDataBase::getAllUserName()
{
	this->_allUsers.clear();
	int i = 0;
	this->_query = "SELECT NAME FROM USERS;";
	int res = sqlite3_exec(this->_db, _query.c_str(), SqliteDataBase::callbackGetAllUsers, &(this->_allUsers), nullptr);
	if (res != SQLITE_OK)
	{
		cout << "Error query" << endl;
	}

	//print result all of user in my game.
	for (i = 0; i < this->_allUsers.size(); i++)
	{
		cout <<this->_allUsers[i]<<endl;
	}
	return this->_allUsers;
}




bool SqliteDataBase::doesUserExist(string userName)
{
	this->initUsers();//init the list of users
	for (auto& user : this->_ListUsers)
	{
		if (userName == user.getName())
		{
			return true;
		}
	}
	return false;
}


bool SqliteDataBase::doesPasswordMatch(string name,string password)
{
	cout << "the name is:" << name<<endl;
	cout << "the password is:" << password<<endl;
	cout << "\n\n"<<endl;
	this->initUsers();//init the list of users
	for (auto& user : this->_ListUsers)
	{
		cout << "name:" << user.getName() << "      password:" << user.getPassword() << endl;
		if (password == user.getPassword() && name == user.getName())
		{
			return true;
		}
	}
	return false;
}


bool SqliteDataBase::addNewUser(string userName, string password, string email)
{
	this->_query = "INSERT INTO USERS VALUES('" + userName + "', '" + password + "', '"  + email + "' );";
	cout << "::::::::::::::::::::::::::::::::::::"<<this->_query << endl;
	int res = sendData(this->_query.c_str(), nullptr);
	if (res != SQLITE_OK){
		return false;}
	return true;
}



User SqliteDataBase::initUserStatus(string name)
{
	this->initUsers();//init the list of users
	for (auto& user : this->_ListUsers)
	{
		if (user.getName() == name)
		{
			// init status user for example: answer time,numOfCorrectAns ...
			this->_query = " SELECT *FROM (SELECT avg(ANSWER_TIME) as 'AvgTimeAns', "
				"count(IS_CURRECT) as 'NumAns', "
				"count(DISTINCT(GAME_ID)) as 'NumOfGmae' FROM statistics "
				"where USER_NAME == '" + name + "' ), "
				"(SELECT  count(statistics.IS_CURRECT)as 'NumCorrectAns' FROM statistics "
				"WHERE IS_CURRECT == 1 AND USER_NAME ==  '" + name + "');";
			cout << "query is:\n" <<this->_query << endl;
			int res = sqlite3_exec(this->_db, _query.c_str(), SqliteDataBase::callbackUserStatus, &user, nullptr);
			if (res != SQLITE_OK)
			{
				cout << "Error query" << endl;
			}
			else
			{ 
				return user;
			}
		}
	}
	throw(string("Not fount this user"));
}


float SqliteDataBase::getPlayerAverageAnswerTime(string name) 
{
	try 
	{
		return (this->initUserStatus(name).status.getAverageAnswerTime());
	}
	catch (const string e)
	{
		cout << e<<endl;
	}
	return 0.0;
}

int SqliteDataBase::getNumOfCorrectAnswers(string name) 
{
	try {
		return (this->initUserStatus(name).status.getCorrectAnswers());
	}
	catch (const string e) {
		cout << e << endl;
	}
	return 0;
}

int SqliteDataBase::getNumOfTotalAnswers(string name) 
{
	try {
		return (this->initUserStatus(name).status.getTotalAnswers());
	}
	catch (const string e) {
		cout << e << endl;
	}	return 0;
}

int SqliteDataBase::getNumOfPlayerGames(string name) 
{
	try {
		return (this->initUserStatus(name).status.getPlayerGames());
	}
	catch (const string e) {
		cout << e << endl;
	}
	return 0;
}

