#pragma once
#include "Helper.h"
#include "Request.h"
using json = nlohmann::json;
using std::string;
using std::vector;

class JsonRequestPacketDeserializer 
{
public :
	static  LoginRequest deserializeLoginRequest(vector <char unsigned > buffer);
	static	SignupRequest deserializeSignupRequest(vector <char unsigned > buffer);
	static  GetPlayersInRoomRequest deserializeGetPlayersRequest(vector <char unsigned > buffer);
	static	JoinRoomRequest deserializeJoinRoomRequest(vector <char unsigned > buffer);
	static  CreateRoomRequest deserializeCreateRoomRequest(vector <char unsigned > buffer);

};
