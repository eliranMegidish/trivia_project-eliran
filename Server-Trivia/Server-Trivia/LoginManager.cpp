#include "LoginManager.h"


LoginManager::LoginManager(IDatabase* m_database):m_database(m_database)
{
	this->m_database->open();
}

bool LoginManager::signup(string name, string password, string email)
{
	if (this->m_database->doesUserExist(name))
	{
		return false;
	}
	else
	{
		if (!this->m_database->addNewUser(name, password, email))
		{
			cout << "Error with query sql"<< endl;
		}
		else
		{
			LoggedUser logged(name);
			this->m_loggedUsers.push_back(logged);
		}
		return true;
	}
}

bool LoginManager::login(string name, string password)
{
	for (LoggedUser user : this->m_loggedUsers)
	{
		if (user.getUserName() == name)
		{
			return false;
		}
	}
	if (this->m_database->doesPasswordMatch(name, password))
	{
		LoggedUser logged(name);
		this->m_loggedUsers.push_back(logged);
		return true;
	}
	else
	{
		return false;
	}
}

bool LoginManager::logout(string name)
{
	int i = 0;
	for (auto& loggedUser : this->m_loggedUsers)
	{
		if (loggedUser.getUserName() == name)
		{
			this->m_loggedUsers.erase(this->m_loggedUsers.begin()+i);
			return true;
		}
		i++;
	}
	return false;
}

void LoginManager::printAllLoggedUser()
{
	int i = 0;
	for (i = 0; i < this->m_loggedUsers.size(); i++)
	{
		cout <<i <<")"<< this->m_loggedUsers[i].getUserName() << endl;
	}
}
