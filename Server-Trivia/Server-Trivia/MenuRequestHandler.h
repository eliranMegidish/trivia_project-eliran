#pragma once
#include "RequestHandlerFactory.h"


class MenuRequestHandler : public IRequestHandler
{

public:	
	MenuRequestHandler(RequestHandlerFactory* handlerFactory, string name);
	bool isRequestRelevant(RequestInfo info)override;
	RequestResult handleRequest(RequestInfo info)override;
	RequestHandlerFactory* getPointerToHandlerFactory();
	string getNameLoggedUser();


private:
	LoggedUser m_user;
	RequestHandlerFactory *m_handlerFactory;
	
	RequestResult signout(RequestInfo info);
	RequestResult getRooms(RequestInfo info);
	RequestResult getPlayersInRoom(RequestInfo info);
	RequestResult getStatistics(RequestInfo info);
	RequestResult joinRoom(RequestInfo info);
	RequestResult createRoom(RequestInfo info);
	RequestResult getHighScores(RequestInfo info);

};