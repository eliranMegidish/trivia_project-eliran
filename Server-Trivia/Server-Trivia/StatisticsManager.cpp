#include "StatisticsManager.h"
#include "Helper.h"

StatisticsManager::StatisticsManager(IDatabase* database)
{
	this->m_database = database;
}

UserStatus StatisticsManager::getStatistics(string name)
{
	UserStatus status;
	status.setCorrectAnswers(this->m_database->getNumOfCorrectAnswers(name));
	status.setAverageAnswerTime(this->m_database->getPlayerAverageAnswerTime(name));
	status.setPlayerGames(this->m_database->getNumOfPlayerGames(name));
	status.setTotalAnswers(this->m_database->getNumOfTotalAnswers(name));
	return status;
}

GetHighScores StatisticsManager::getHighScores()
{
	GetHighScores highScores;
	//query that get the all user name in my game .
	for (string name : this->m_database->getAllUserName())
	{
		highScores.highScoresUsers[this->getStatistics(name).getTotalStatistics()] = name;// insert value to the map
	}
	return highScores;
}
