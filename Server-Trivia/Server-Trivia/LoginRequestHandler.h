#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;
class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(RequestHandlerFactory* handlerFactory);
	bool isRequestRelevant(RequestInfo info) override;
	RequestResult  handleRequest(RequestInfo info) override;
	RequestHandlerFactory* getPointerToHandlerFactory();



private:
	RequestHandlerFactory* m_handlerFactory;
	RequestResult login(RequestInfo info);
	RequestResult signup(RequestInfo info);


};
