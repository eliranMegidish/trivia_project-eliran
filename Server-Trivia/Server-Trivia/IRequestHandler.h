#pragma once
#include<iostream>
#include "Helper.h"
#include "Request.h"


class RequestHandlerFactory;
class IRequestHandler 
{
public:
	virtual bool isRequestRelevant(RequestInfo info ) = 0 ;
	virtual RequestResult handleRequest(RequestInfo info) = 0;
	virtual  RequestHandlerFactory* getPointerToHandlerFactory() = 0;


};

