#pragma once

#include <map>
#include "Room.h"

using std::map;



class RoomManager 
{
public:
	
	void createRoom(LoggedUser loggedUser , string nameRoom, int maxPlayers, unsigned int timePerQuestion, unsigned int numberOfQuestion);
	void deleteRoom(int ID);
	unsigned int getRoomState(int ID);
	vector<RoomData> getRooms();
	vector<string> getNamePlayersInRoom(unsigned int roomId);
	bool addPlayerToRoomByRoomId(unsigned int roomId , LoggedUser m_user);
private:                         
	static int _currentId;
	map<int unsigned, Room> m_rooms;
};