#include "RoomManager.h"

int RoomManager::_currentId = 0;

/*
When room created the user that craeted the room is now in the room.
*/
void RoomManager::createRoom(LoggedUser loggedUser, string nameRoom, int maxPlayers, unsigned int timePerQuestion, unsigned int numberOfQuestion)
{
	Room room;
	room.setRoomData(++this->_currentId,nameRoom, maxPlayers, timePerQuestion, numberOfQuestion,1);
	//room.setId(++this->_currentId);//init roomData Struct
	room.addUser(loggedUser);
	this->m_rooms[room.getStructRoomData()._id] = room;
}

/*
Delete room from map by ID.
*/
void RoomManager::deleteRoom(int ID)
{
	this->m_rooms.erase(ID);
}


unsigned int RoomManager::getRoomState(int ID)
{
	return 	this->m_rooms[ID].getStructRoomData()._isActive;
}


vector<RoomData> RoomManager::getRooms()
{
	vector<RoomData> roomData;
	for (auto room : this->m_rooms){
		roomData.push_back(room.second.getStructRoomData());
	}
	return roomData;
}

vector<string> RoomManager::getNamePlayersInRoom(unsigned int roomId)
{
	return 	m_rooms[roomId].getAllUsers();
}

bool RoomManager::addPlayerToRoomByRoomId(unsigned int roomId , LoggedUser m_user)
{
	return this->m_rooms[roomId].addUser(m_user);
}
