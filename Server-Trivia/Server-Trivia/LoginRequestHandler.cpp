#include "LoginRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "SqliteDataBase.h"


LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* handlerFactory) :m_handlerFactory(handlerFactory)
{
	//cout << "1)m_database:" << m_handlerFactory->m_database << endl;
}



bool LoginRequestHandler::isRequestRelevant(RequestInfo info)
{
	if (info.id == CODE_CLIENT_LOGIN || info.id == CODE_CLIENT_SIGNUP)
	{
		return true;
	}
	return false;
}


/*
this function get the requist from client and return request result
*/
RequestResult LoginRequestHandler::handleRequest(RequestInfo info)
{
	RequestResult result;// response that will be after serializer data

	//LoginRequestHandler* loginHandler = new  LoginRequestHandler(this->m_handlerFactory->createLoginRequestHandler());// the next handler //�� ������� ������ ���� � ����� �� ����� ���� ����� ��� ����� ����

	if (info.id == CODE_CLIENT_LOGIN)
	{
		result = this->login(info);
	}
	else if (info.id == CODE_CLIENT_SIGNUP)
	{
		result = this->signup(info);
	}
	return result;
}


/*
Login request:
In this function i get the request form client and first I do Deserializer to request
and return RequestResult that contain the response form server
*/
RequestResult LoginRequestHandler::login(RequestInfo info)
{
	LoginResponse login_response;
	LoginRequest login_request;
	RequestResult result;
	// do Deserializer to request
	login_request = JsonRequestPacketDeserializer::deserializeLoginRequest(info.msgFromClient);

	cout << login_request.password << endl;
	cout << login_request.userName << endl;

	if (this->m_handlerFactory->getLoginManager().login(login_request.userName, login_request.password))
	{
		login_response.status = 1;
		result.response = JsonResponsePacketSerializer::serializeResponse(login_response);
		result.newHandler = new MenuRequestHandler(this->m_handlerFactory->createMenuRequestHandler(login_request.userName));
		delete this; // delete the memory thay I allocate in -> acceptClient function 
	}
	else
	{
		login_response.status = 0;
		result.response = JsonResponsePacketSerializer::serializeResponse(login_response);
		result.newHandler = this;// cannot succeed to login or sign up need stay in the same situation 
	}
	return result;
}

RequestResult LoginRequestHandler::signup(RequestInfo info)
{
	SignupResponse signup_response;
	SignupRequest signup_request;
	RequestResult result;
	// do Deserializer to request
	signup_request = JsonRequestPacketDeserializer::deserializeSignupRequest(info.msgFromClient);
	cout << signup_request.password << endl;
	cout << signup_request.userName << endl;
	cout << signup_request.email << endl;

	if (this->m_handlerFactory->getLoginManager().signup(signup_request.userName, signup_request.password, signup_request.email))
	{
		signup_response.status = 1;
		result.response = JsonResponsePacketSerializer::serializeResponse(signup_response);
		result.newHandler = new MenuRequestHandler(this->m_handlerFactory->createMenuRequestHandler(signup_request.userName));
		delete this;// delete the memory thay I allocate in -> acceptClient function 
	}
	else
	{
		signup_response.status = 0;
		result.response = JsonResponsePacketSerializer::serializeResponse(signup_response);
		result.newHandler = this;// cannot succeed to login or sign up need stay in the same situation
	}
	return result;
}


RequestHandlerFactory* LoginRequestHandler::getPointerToHandlerFactory()
{
	return this->m_handlerFactory;
}