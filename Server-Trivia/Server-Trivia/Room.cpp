#include "Room.h"

RoomData Room::getStructRoomData()
{
	return this->m_metadata;
}

/*
This function initializes a struct named- RoomData.
*/
void Room::setRoomData(unsigned int id,string nameRoom, int maxPlayers, unsigned int timePerQuestion, unsigned int numberOfQuestion,unsigned int isActive)
{
	this->m_metadata._id = id;
	this->m_metadata._name = nameRoom;
	this->m_metadata._maxPlayers = maxPlayers;
	this->m_metadata._timePerQuestion = timePerQuestion;
	this->m_metadata._numberOfQuestion = numberOfQuestion;
	this->m_metadata._isActive = isActive;
}

void Room::setId(unsigned int id)
{
	this->m_metadata._id = id;
}

void Room::setIsActive(unsigned int flag)
{
	this->m_metadata._isActive=flag;
}

//this function add user to room (push logged user to vector:).
bool Room::addUser(LoggedUser user)
{
	if (this->m_metadata._isActive == 1)
	{
		this->m_users.push_back(user);
		return true;
	}
	else
	{
		return false;
	}
}

/*
this function remove specific objecct from vector -> the vector contian the palyer that in the room.
*/
void Room::removeUser(LoggedUser user)
{
	int i = 0;
	for (i = 0; i < this->m_users.size(); i++)
	{
		if (m_users[i].getUserName() == user.getUserName())
		{
			this->m_users.erase(this->m_users.begin() + i);
		}
	}
}

/*
This function return list that contain all name of users that in the room 
*/
vector<string> Room::getAllUsers()
{
	vector <string> allUserName;
	for (LoggedUser user : this->m_users)
	{
		allUserName.push_back(user.getUserName());
	}
	return allUserName;
}
