#include "MenuRequestHandler.h"
#include "Resopnse.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"

MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory* handlerFactory,string name):m_handlerFactory(handlerFactory),m_user(name)
{
	cout << "welcome:" << endl;
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo info)
{
	if (info.id == CODE_CLIENT_CREATE_ROOM || info.id == CODE_CLIENT_ROOM_LIST || info.id == CODE_CLIENT_JOIN_ROOM || info.id == CODE_CLIENT_PLAYER_IN_ROOM || info.id == CODE_CLIENT_LOGOUT || info.id==CODE_CLIENT_STATISTICS || info.id==CODE_CLIENT_HIGH_SCORES)
	{
		std::cout << "is Request Relevant:" << info.id << std::endl;
		return true;
	}
	return false;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo info)
{
	RequestResult result;
	if (info.id == CODE_CLIENT_LOGOUT)
	{
		result = this->signout(info);
	}
	else if (info.id == CODE_CLIENT_CREATE_ROOM)
	{
		result = this->createRoom(info);
	}
	else if (info.id == CODE_CLIENT_ROOM_LIST)
	{
		result = this->getRooms(info);
	}
	else if (info.id == CODE_CLIENT_JOIN_ROOM)
	{
		result=this->joinRoom(info);
	}
	else if (info.id == CODE_CLIENT_PLAYER_IN_ROOM)
	{
		result = this->getPlayersInRoom(info);
	}
	else if (info.id == CODE_CLIENT_STATISTICS)
	{
		result = this->getStatistics(info);
	}
	else if (info.id == CODE_CLIENT_HIGH_SCORES)
	{
		result = this->getHighScores(info);
	}
	//....
	return result;
}

RequestHandlerFactory* MenuRequestHandler::getPointerToHandlerFactory()
{
	return this->m_handlerFactory;
}

string MenuRequestHandler::getNameLoggedUser()
{
	return this->m_user.getUserName();
}

/*
here i need to delete memory
*/
RequestResult MenuRequestHandler::signout(RequestInfo info)
{
	RequestResult result;
	LogoutResponse logoutResponse;
	

	if (this->m_handlerFactory->getLoginManager().logout(this->getNameLoggedUser()))
	{
		//cout << "logout" << endl;
		logoutResponse.status = 1; //
		result.response = JsonResponsePacketSerializer::serializeResponse(logoutResponse);
		result.newHandler = new LoginRequestHandler(this->m_handlerFactory->createLoginRequestHandler());//return back to login 
		delete this; // free the danimic memory. //����� �����  return - �� ��� ���� �� ������ �
	}
	else
	{
		//cout << "canot logout" << endl;
		logoutResponse.status = 0; //
		result.response = JsonResponsePacketSerializer::serializeResponse(logoutResponse);
		result.newHandler = this;
	}
	return result;
}

RequestResult MenuRequestHandler::getRooms(RequestInfo info)
{
	GetRoomsResponse getRoomsResponse;
	RequestResult result;

	getRoomsResponse.rooms=this->m_handlerFactory->getRoomManager().getRooms(); //init the struct.

	if (getRoomsResponse.rooms.empty())// if no has room 
	{
		getRoomsResponse.status = 0;
		result.newHandler = this;
	}
	else
	{
		getRoomsResponse.status = 1;
		result.newHandler = this;
	}
	result.response = JsonResponsePacketSerializer::serializeResponse(getRoomsResponse);
	return result;
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo info)
{

	GetPlayersInRoomResponse playersInRoomResponse;
	GetPlayersInRoomRequest playersInRoomRequest;
	RequestResult result;

	playersInRoomRequest = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(info.msgFromClient);
	playersInRoomResponse.names = this->m_handlerFactory->getRoomManager().getNamePlayersInRoom(playersInRoomRequest.roomId);
	result.response = JsonResponsePacketSerializer::serializeResponse(playersInRoomResponse);
	result.newHandler = this;
	return result;
}



RequestResult MenuRequestHandler::joinRoom(RequestInfo info)
{
	JoinRoomResponse joinRoomResponse;
	JoinRoomRequest joinRoomRequest;
	RequestResult result;

	joinRoomRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(info.msgFromClient);

	if (this->m_handlerFactory->getRoomManager().addPlayerToRoomByRoomId(joinRoomRequest.roomId, this->m_user))
	{
		joinRoomResponse.status = true;
		//result.newHandler = ; //������ �����
	}
	else
	{
		joinRoomResponse.status = false;
		result.newHandler = this;
	}
	result.response = JsonResponsePacketSerializer::serializeResponse(joinRoomResponse);
	return result;
}

RequestResult MenuRequestHandler::createRoom(RequestInfo info)
{
	CreateRoomRequest createRoomRequest;
	CreateRoomResponse createRoomResponse;
	RequestResult result;

	createRoomRequest = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(info.msgFromClient);
	this->m_handlerFactory->getRoomManager().createRoom(this->m_user, createRoomRequest.roomName,createRoomRequest.maxUsers, createRoomRequest.answerTimeout,createRoomRequest.questionCount);
	createRoomResponse.status = true;
	result.response = JsonResponsePacketSerializer::serializeResponse(createRoomResponse);
	result.newHandler = this;
	return result;
}

RequestResult MenuRequestHandler::getStatistics(RequestInfo info)
{
	UserStatus userStatus = this->m_handlerFactory->getStatisticsManager().getStatistics(this->m_user.getUserName());
	RequestResult result;
	result.response = JsonResponsePacketSerializer::serializeResponse(userStatus);
	result.newHandler = this;
	return result;
}

RequestResult MenuRequestHandler::getHighScores(RequestInfo info)
{
	GetHighScores highScores = this->m_handlerFactory->getStatisticsManager().getHighScores();
	RequestResult result;

	result.response = JsonResponsePacketSerializer::serializeResponse(highScores);
	result.newHandler = this;

	return result;
}
