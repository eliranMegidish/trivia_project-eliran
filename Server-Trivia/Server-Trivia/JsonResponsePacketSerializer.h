#pragma once
#include "Helper.h"
#include "Resopnse.h"
#include "UserStatus.h"


class JsonResponsePacketSerializer
{
public:
	static std::vector<char unsigned> serializeResponse(ErrorResponse error);
	static std::vector<char unsigned> serializeResponse(LoginResponse login);
	static std::vector<char unsigned> serializeResponse(SignupResponse Signup);
	static std::vector<char unsigned> serializeResponse(LogoutResponse logout);
	static std::vector<char unsigned> serializeResponse(GetRoomsResponse roomsData);
	static std::vector<char unsigned> serializeResponse(GetPlayersInRoomResponse playersInRoom);
	static std::vector<char unsigned> serializeResponse(JoinRoomResponse JoinRoom);
	static std::vector<char unsigned> serializeResponse(CreateRoomResponse createRoom);
	static std::vector<char unsigned> serializeResponse(UserStatus userStatus);
	static std::vector<char unsigned> serializeResponse(GetHighScores highScores);



};


