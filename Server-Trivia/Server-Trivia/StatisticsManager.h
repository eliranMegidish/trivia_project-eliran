#pragma once
#include "IDatabase.h"
#include "UserStatus.h"
#include "Resopnse.h"


class StatisticsManager 
{
public:
	StatisticsManager(IDatabase* database);
	UserStatus getStatistics(string name);
	GetHighScores getHighScores();
private:
	IDatabase* m_database;
};