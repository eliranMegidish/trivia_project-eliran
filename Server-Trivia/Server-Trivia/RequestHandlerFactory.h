#pragma once
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RequestHandlerFactory 
{
public:
	RequestHandlerFactory(IDatabase* m_database);
	LoginRequestHandler createLoginRequestHandler();
	LoginManager& getLoginManager();

	MenuRequestHandler createMenuRequestHandler(string name);
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();
	IDatabase* m_database;

private:
	LoginManager  m_loginManager;
	RoomManager m_roomManager;
	StatisticsManager m_StatisticsManager;

};