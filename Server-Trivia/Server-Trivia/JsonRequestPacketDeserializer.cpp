#include "JsonRequestPacketDeserializer.h"


/*
here I get only message from client in format binary and i convert this to string 
and after that i convert the string to json forma.
*/
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(vector<char unsigned> buffer)
{
	LoginRequest login;
	json j = Helper::bin_to_string_to_json(buffer);//bin->string->json

	std::cout << "#######################" << std::endl;
	std::cout << j << std::endl;


	login.userName =  j["username"].get<string>();
	login.password =  j["password"].get<string>();
	return login;
}


SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(vector<char unsigned>buffer)
{
	SignupRequest signup;
	json j = Helper::bin_to_string_to_json(buffer);
	signup.userName =  j["username"].get<string>();
	signup.password =  j["password"].get<string>();
	signup.email    =  j["email"].get<string>();
	return signup;
}


GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(vector<char unsigned> buffer)
{
	GetPlayersInRoomRequest playersInRoom;
	json j = Helper::bin_to_string_to_json(buffer);
	playersInRoom.roomId = j["roomId"];
	return playersInRoom;
}


JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(vector<char unsigned> buffer)
{
	JoinRoomRequest joinRoom;
	json j = Helper::bin_to_string_to_json(buffer);
	joinRoom.roomId = j["roomId"];
	return joinRoom;
}


CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(vector<char unsigned> buffer)
{
	CreateRoomRequest createRoom;
	json j = Helper::bin_to_string_to_json(buffer);
	createRoom.roomName=j["roomName"].get<string>();
	createRoom.maxUsers = j["maxUsers"];
	createRoom.questionCount = j["questionCount"];
	createRoom.answerTimeout = j["answerTimeout"];
	return createRoom;
}

