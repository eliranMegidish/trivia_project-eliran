#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include <iostream>
#include "Server.h"
#include "SqliteDataBase.h"


int main()
{
	try
	{
		std::cout<<("Starting...")<<endl;
		WSAInitializer wsa_init;
		Server md_server(new SqliteDataBase);
		md_server.run();
	}
	catch (const std::exception & e)
	{
		std::cout << "Exception was thrown in function: " << e.what() << std::endl;
	}
}
