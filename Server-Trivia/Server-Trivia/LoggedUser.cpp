#include "LoggedUser.h"

LoggedUser::LoggedUser(string name):m_username(name)
{
}

string LoggedUser::getUserName()
{
	return this->m_username;
}

void LoggedUser::setUserName(string name)
{
	this->m_username = name;
}
