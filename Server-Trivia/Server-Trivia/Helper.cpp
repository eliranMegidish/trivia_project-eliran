#include "Helper.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

using std::string;
using json = nlohmann::json;

/*
Convert string to vector 
and return vector in formt binary .
*/
std::vector<char unsigned> Helper::getVectorMsg(std::string s)
{
	std::vector<char unsigned> data;
	std::copy(s.begin(), s.end(), std::back_inserter(data));
	return data;
}


/*
Get  from all data only 8 first char that represent code msg
*/
string Helper::getMessageCode(string s)
{
	return s.erase(LEN_CODE_MSG);
}


/*
Here i want to get only message in fromat json from all of data that client send to server
return - string that contain the message in format _binary_.
*/
std::string Helper::getMessage(string buffer)
{
	return buffer.erase(0,LEN_CODE_MSG+ LEN_CODE_MSG*4); //remove the 8+32=40 begin char
}


// return string after padding zeros if necessary
string Helper::getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();
}


// Send data to socket
// this is private function
void Helper::sendData(SOCKET sc, string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}


/*
This function get 3 parmeter (codeMsg,lenMsg,msg) and create the full response that send to client (the response in the format binary!)
parmeters:
codeMsg- type int (number (i need to convert this to binary))--code message that server send to client  
lenMsg- type int - number that represent the lenght of var "msg"
msg - type string in format binary that contain the data in formt binary that server need to send to client 

I use with this function when I wnat to serialize* the data
*/
std::string Helper::create_response_msg_from_sever(const int codeMsg, const int lenMsg, const string msg)
{
	//string response = "";
	std::cout << "_!_!_!!!__!!_!_!_!_!_!?????????????"<< std::bitset<8>(codeMsg).to_string() << std::endl;
	//response = std::bitset<8>(codeMsg).to_string() + getPaddedNumber(std::stoi(std::bitset<8>(lenMsg).to_string()), LEN_CODE_MSG * 4) + msg;
	return std::bitset<8>(codeMsg).to_string() + getPaddedNumber(std::stoi(std::bitset<8>(lenMsg).to_string()), LEN_CODE_MSG * 4) + msg;
}


string Helper::stringToBinary(string msg)
{
	string binaryString = "";
	for (char& _char : msg) //���� �� �� ������ �� ������� 
	{
		binaryString += std::bitset<8>(_char).to_string();
	}
	return binaryString;
}


/*
convert binary string to string text
*/
string Helper::binaryToString(string binaryString)
{
	
	string text = "";
	std::stringstream sstream(binaryString);
	while (sstream.good())
	{
		std::bitset<8> bits;
		sstream >> bits;
		text += char(bits.to_ulong());
	}
	//std::cout << text << std::endl;
	return text;
}


/*
This funtion get vector that contin requist in binary 
and return format json data
*/
nlohmann::json Helper::bin_to_string_to_json(std::vector<char unsigned> binaryRequest)
{
	std::string msg_from_client_json(binaryRequest.begin(), binaryRequest.end());// convert vector to string ->>string binary message
	std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
	std::cout << binaryToString(msg_from_client_json) << std::endl;
	return nlohmann::json::parse(Helper::binaryToString(msg_from_client_json)); //return -string to json
}



/*
this function get string that contain json in format string 
and return vector <char unsigned> that  contain the response from server to the client.
and this function send to function "create_response_msg_from_sever" 
*/
std::vector<char unsigned> Helper::serialize(std::string buffer, int const codeMsg)//get string in formt strint-json 
{
	std::vector<char unsigned> response;
	buffer = Helper::stringToBinary(buffer);// convert json format string to string in format binary
	buffer = Helper::create_response_msg_from_sever(codeMsg,buffer.size(), buffer);
	//std::cout << "the buffer is :" << buffer<< std::endl;
	std::copy(buffer.begin(), buffer.end(), std::back_inserter(response));

	return response;
}


std::string Helper::vectorToString(std::vector<char unsigned> vec)
{
	return string(vec.begin(),vec.end());
}






