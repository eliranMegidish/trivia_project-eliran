#pragma once
#include "IDatabase.h"
#include "LoggedUser.h"
#include <vector>

using std::vector;

class LoginManager 
{
public:
	LoginManager(IDatabase* m_database);
	bool signup(string name, string password, string email);//����� 
	bool login(string name, string password);// �������
	bool logout(string name);// ����� 
	void printAllLoggedUser();

private:
	IDatabase* m_database;
	vector<LoggedUser>m_loggedUsers;
};