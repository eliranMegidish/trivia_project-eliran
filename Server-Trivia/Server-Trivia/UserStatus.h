#pragma once
#include <iostream>



class UserStatus 
{


public:
	//set
	void setAverageAnswerTime(float averageAnswerTime);
	void setCorrectAnswers(int correctAnswers);
	void setTotalAnswers(int totalAnswers);
	void setPlayerGames(int playerGames);
	//get
	float getAverageAnswerTime();
	int getCorrectAnswers();
	int getTotalAnswers();
	int getPlayerGames();
	float getTotalStatistics();
		
private:
	float _averageAnswerTime;
	int _correctAnswers;
	int _totalAnswers;
	int _playerGames;
};
