#pragma once
#include <list>
#include "IDatabase.h"
#include "User.h"
#include <vector>

class SqliteDataBase :public IDatabase
{

public:

	bool open() override;
	bool doesUserExist(string userName)override;
	bool doesPasswordMatch(string name,string password) override;
	bool addNewUser(string userName, string password, string email)override;

	float getPlayerAverageAnswerTime(string name) override;
	int getNumOfCorrectAnswers(string name) override;
	int getNumOfTotalAnswers(string name) override;
	int getNumOfPlayerGames(string name) override;
	std::vector<std::string> getAllUserName() override;

	User initUserStatus(string name);
	void printUsers();


private:
	bool sendData(const char* query, int (*callback)(void*, int, char**, char**));
	static int callbackUsers(void* data, int argc, char** argv, char** azColName);
	static int callbackUserStatus(void* data, int argc, char** argv, char** azColName);
	static int callbackGetAllUsers(void* data, int argc, char** argv, char** azColName);
	void initUsers();

	sqlite3* _db;
	std::string _dbFileName = "sqlliteDb.db";
	std::list <User> _ListUsers;//login user 
	std::string _query;
	std::vector<string> _allUsers;// all user in database

};
