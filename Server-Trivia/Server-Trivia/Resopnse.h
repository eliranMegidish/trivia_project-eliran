#pragma once
#include <iostream>
#include <map>
#include "Room.h"

typedef struct ErrorResponse
{
	std::string message;

}ErrorResponse;

typedef struct LoginResponse
{
	unsigned int status;

}LoginResponse;

typedef struct SignupResponse
{
	unsigned int status;

}SignupResponse;

typedef struct LogoutResponse
{
	unsigned int status;

}LogoutResponse;

typedef struct GetRoomsResponse
{
	unsigned int status;
	vector<RoomData> rooms;

}GetRoomsResponse;

typedef struct GetPlayersInRoomResponse
{
	vector<string>names;

}GetPlayersInRoomResponse;

typedef struct GetStatisticsResponse
{
	unsigned int status;
	vector<string> statistics;

}GetStatisticsResponse;

typedef struct JoinRoomResponse
{
	unsigned int status;

}JoinRoomResponse;

typedef struct CreateRoomResponse
{
	unsigned int status;

}CreateRoomResponse;

typedef struct GetHighScores
{
	std::map<float,string> highScoresUsers;
}GetHighScoress;

