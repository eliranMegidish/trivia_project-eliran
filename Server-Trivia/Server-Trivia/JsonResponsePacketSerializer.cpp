#include "JsonResponsePacketSerializer.h"
using json = nlohmann::json;
using std::string;


std::vector<char unsigned> JsonResponsePacketSerializer::serializeResponse(ErrorResponse error)
{
    json j ;
    j["message"] = error.message;    // convert format struct to json
	return Helper::serialize(j.dump(), CODE_SERVER_ERROR);// return in msg in format [code,lenMsg,msg]
}


std::vector<char unsigned> JsonResponsePacketSerializer::serializeResponse(LoginResponse login)
{
    json j;
    j["status"] = login.status;  // convert format struct to json
   // std::cout <<"***"<<Helper::serialize(j.dump(), SERVER_LOGIN)<<std::endl;
    return Helper::serialize(j.dump(), CODE_SERVER_LOGIN);// send to function json in format string
}


std::vector<char unsigned> JsonResponsePacketSerializer::serializeResponse(SignupResponse Signup)
{
    json j;
    j["status"] = Signup.status;// convert format struct to json
    return Helper::serialize(j.dump(), CODE_SERVER_SIGNUP);
}


std::vector<char unsigned> JsonResponsePacketSerializer::serializeResponse(LogoutResponse logout)
{
    json j;
    j["status"] = logout.status;// convert format struct to json
    return Helper::serialize(j.dump(), CODE_SERVER_LOGOUT);
}


std::vector<char unsigned> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse roomsData)
{
    json j;
    j["status"] = roomsData.status; // if the request ������ 
    for (auto& room : roomsData.rooms){
        j["rooms"].push_back(room._name);
    }
    std::cout << "**__json get romms__**" << std::endl;
    return Helper::serialize(j.dump(), CODE_SERVER_GET_ROOMS);
}


std::vector<char unsigned> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse playersInRoom)
{
    json j;
    for (auto room : playersInRoom.names){
        j["PlayersInRoom"].push_back(room);
    }
    return Helper::serialize(j.dump(), CODE_SERVER_GET_PLAYERS_IN_ROOM);
}


std::vector<char unsigned> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse JoinRoom)
{
    json j;
    j["status"] = JoinRoom.status;// convert format struct to json
    return Helper::serialize(j.dump(), CODE_CLIENT_JOIN_ROOM);
}


std::vector<char unsigned> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse createRoom)
{
    json j;
    j["status"] = createRoom.status;// convert format struct to json
    return Helper::serialize(j.dump(), CODE_SERVER_CREATE_ROOM);
}

std::vector<char unsigned> JsonResponsePacketSerializer::serializeResponse(UserStatus statistics)
{
    json j;
    j["Average_Answer_Time"] = statistics.getAverageAnswerTime();
    j["Correct_Answers"] = statistics.getCorrectAnswers();
    j["Player_Games"] = statistics.getPlayerGames();
    j["Total_Answers"] = statistics.getTotalAnswers();
    return Helper::serialize(j.dump(), CODE_SERVER_STATISTIC);
}

std::vector<char unsigned> JsonResponsePacketSerializer::serializeResponse(GetHighScores highScores)
{

    json j;
    int i = 0;
    for (auto& it : highScores.highScoresUsers) 
    {
        if (i < 3)
        {
            j["names"].push_back(it.second);
            j["scores"].push_back(it.first);
            i++;
        }
    }
    return  Helper::serialize(j.dump(), CODE_SERVER_HIGH_SCORES);
}
