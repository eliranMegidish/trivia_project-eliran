#pragma once
#include "UserStatus.h"

using std::string;
using std::endl;
using std::cout;


class User {

public:
	User();
	void setName(string name);
	void setPassword(string password);
	void setEmail(string email);

	string getName();
	string getPassword();
	string getEmail();
	UserStatus status;

private:
	friend std::ostream& operator<<(std::ostream& str, const User& user);
	string name;
	string password;
	string email;
};