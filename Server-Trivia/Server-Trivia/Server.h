#pragma once
#include "Communicator.h"


class Server 
{

public:
	Server(IDatabase* m_database);
	void run();

private:
	// ���� ��� �� ������� ��� ���� ���� ����� ����� �� ������ �� ������ ���� ����� ������ ����� ���
	RequestHandlerFactory RequestHandlerFactory;
	Communicator m_communicator;
	IDatabase *m_database;
};