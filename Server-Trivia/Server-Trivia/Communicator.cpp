#include "Communicator.h"
#include <exception>

static const unsigned short PORT = 8826;
static const unsigned int IFACE = 0;
using std::cout;
using std::endl;

Communicator::Communicator(RequestHandlerFactory handlerFactory):m_handlerFactory(handlerFactory)//���� ����� 
{
	cout << "3)Communicator" << this->m_handlerFactory.m_database << endl;
	this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->_socket == INVALID_SOCKET)
	{throw std::exception(__FUNCTION__ " - socket");}
}

Communicator::~Communicator()
{
	try
	{
		cout <<"***********----------*********////////////***********----------*******"<<  endl;
		delete this->m_clients[this->_socket];
		closesocket(this->_socket);
	}
	catch (...) {}
}


void Communicator::startHandleRequests()
{
	bindAndListen();
	while (true)
	{
		std::cout << ("accepting client...")<<endl;;
		acceptClient();
	}
}


void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	cout << ("binded");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << PORT << std::endl;
}


void Communicator::acceptClient()
{
	SOCKET client_socket = accept(this->_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << ("Client accepted !") << endl;
	// create new thread for client	and detach from it
	
	LoginRequestHandler* login = new LoginRequestHandler(this->m_handlerFactory.createLoginRequestHandler());
	cout << "address:" <<login<< endl;
	this->m_clients[client_socket] = login;

	std::thread tr(&Communicator::handleNewClient, this, client_socket);
	tr.detach();
}


void Communicator::handleNewClient(SOCKET clientSocket)
{
	char request[1024] = {NULL};
	string sendMsg = "hello";
	RequestResult result;
	ErrorResponse error = { "Error is request not relevant" };

	try
	{
		while (strcmp(request, "EXIT"))
		{
			memset(request, 0, sizeof(request));//clear request 
			recv(clientSocket, request, 1024-1, 0);
			request[1024-1] = '\0';
			//std::cout << "Client request is: " << request << std::endl;
			// do deserialize 
			RequestInfo info = {stoi(Helper::getMessageCode(request),nullptr, 2), time(0),Helper::getVectorMsg(Helper::getMessage(request))}; // init struct RequestInfo (init msg client and codeMsg) 
			//cout << "code:\n" << info.id << endl;
			//cout << "___________________________________________________________" << endl;
			//cout << "data:\n" << Helper::vectorToString(info.msgFromClient) << endl;
			//cout << "___________________________________________________________" << endl;

			//cout << "time:"<<info.receivalTime << endl;


			if (this->m_clients[clientSocket]->isRequestRelevant(info)) // if the request is login or logout  is relvant
			{
				//std::cout << (Helper::vectorToString((this->m_clients[clientSocket]->handleRequest(info).response)))<< std::endl; //response
				// sned to client response from server after serialize

				result = this->m_clients[clientSocket]->handleRequest(info);//get the result of the request
				sendMsg = Helper::vectorToString(result.response);
				//cout << "responseMsgServer:\n" << sendMsg << endl;
				//cout << "___________________________________________________________"  << endl;
				send(clientSocket, sendMsg.c_str(), sendMsg.size(), 0);//send response in binary  
				this->m_clients[clientSocket] = result.newHandler; //the next handler

				//send(clientSocket, Helper::vectorToString((this->m_clients[clientSocket]->handleRequest(info).response)).c_str(), sendMsg.size(), 0);
			}
			else
			{
				std::cout << error.message << std::endl;
				send(clientSocket, error.message.c_str(), error.message.size(), 0);
			}
			this->m_handlerFactory.getLoginManager().printAllLoggedUser();
		}
		sendMsg = "Bye";
		send(clientSocket, sendMsg.c_str(), sendMsg.size(), 0);
		closesocket(clientSocket);// Closing the socket (in the level of the TCP protocol)
	}
	catch (const std::exception & e)
	{
		std::cout << "*********************close Socket*******************";
		delete this->m_clients[clientSocket];
		closesocket(clientSocket);
	}
}

