#include "RequestHandlerFactory.h"



/*
Here I init the constructor object of type LoginManager
and my varubale m_database
*/
RequestHandlerFactory::RequestHandlerFactory(IDatabase* m_database):m_database(m_database), m_loginManager(m_database), m_StatisticsManager(m_database)
{
	//cout << "2)!!!!1RequestHandlerFactory -> m_database " <<m_database << endl;
}

LoginRequestHandler RequestHandlerFactory::createLoginRequestHandler()
{
	LoginRequestHandler loginRequestHandler(this);
	return loginRequestHandler;
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
	return this->m_loginManager;
}


MenuRequestHandler RequestHandlerFactory::createMenuRequestHandler(string name)
{
	return MenuRequestHandler(this,name);
}


StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return this->m_StatisticsManager;
}


RoomManager& RequestHandlerFactory::getRoomManager()
{
	return this->m_roomManager;
}
