#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <map>
#include <thread>
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"




class Communicator
{
public:
	Communicator(RequestHandlerFactory handlerFactory);
	~Communicator();
	void startHandleRequests();
		
private:
	void handleNewClient(SOCKET clientSocket);
	void bindAndListen();
	void acceptClient();

	std::map< SOCKET, IRequestHandler*> m_clients;
	SOCKET _socket;
	JsonResponsePacketSerializer responseSerializer;

	RequestHandlerFactory m_handlerFactory;
};

