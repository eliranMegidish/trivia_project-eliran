#include "Server.h"

Server::Server(IDatabase* m_database):m_database(m_database),m_communicator(RequestHandlerFactory),RequestHandlerFactory(m_database)
{
	cout << "server -> m_database "<< m_database <<endl;
}

void Server::run()
{
	this->m_communicator.startHandleRequests();
}
