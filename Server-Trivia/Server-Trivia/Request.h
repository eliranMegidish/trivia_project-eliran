#pragma once
#include <iostream>
#include <ctime>

using std::string;
class IRequestHandler;

typedef struct LoginRequest
{
	string userName;
	string password;

}LoginRequest;


typedef struct SignupRequest
{
	string userName;
	string password;
	string email;

}SignupRequest;


typedef struct RequestInfo
{
	int  id; //(msgCode)
	time_t receivalTime;
	std::vector<char unsigned>  msgFromClient; // only msg not len mag and code msg// in format bianry

}RequestInfo;


typedef struct RequestResult
{
	IRequestHandler* newHandler;
	std::vector<char unsigned> response;

}RequestResult;


typedef struct GetPlayersInRoomRequest
{
	unsigned int roomId;
}GetPlayersInRoomRequest;


typedef struct JoinRoomRequest
{
	unsigned int roomId;
}JoinRoomRequest;


typedef struct CreateRoomRequest
{
	string roomName;;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
}CreateRoomRequest;





