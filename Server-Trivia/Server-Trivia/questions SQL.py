import requests
import sqlite3

flag = False
conn = sqlite3.connect('sqlliteDb.db')
cursor = conn.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='QUESTION'")#here i check if the table in dadaBase
for row in cursor:
    flag = True

if not(flag):
    print("creating table")
    conn.execute('''CREATE TABLE QUESTION
                (question_id    INTEGER PRIMARY KEY autoincrement NOT NULL,
                 question       TEXT  NOT NULL,
                 correct_ans    TEXT  NOT NULL,
                 ans2           TEXT  NOT NULL,
                 ans3           TEXT  NOT NULL,
                 ans4           TEXT  NOT NULL );''')

number_of_question = "10"

result=requests.get('https://opentdb.com/api.php?amount='+number_of_question+'10&category=12&difficulty=easy&type=multiple')

for i in range(0,int(number_of_question)):
    conn.execute("INSERT INTO QUESTION (question,correct_ans,ans2,ans3,ans4)" +
                 'VALUES ("' + result.json()["results"][i]["question"] + '" , "' +result.json()["results"][i]["correct_answer"]+
                 '" , "' +result.json()["results"][i]["incorrect_answers"][0] + '" , "' +result.json()["results"][i]["incorrect_answers"][1] +
                 '" , "'+result.json()["results"][i]["incorrect_answers"][2] + '" )')
    conn.commit()
    print ("Records created successfully")
    #print(result.json()["results"][i]["question"])
    #print(result.json()["results"][i]["incorrect_answers"][0])
    #print(result.json()["results"][i]["incorrect_answers"][1])
    #print(result.json()["results"][i]["incorrect_answers"][2])
    #print(result.json()["results"][i]["correct_answer"])
