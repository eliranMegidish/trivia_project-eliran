#include "UserStatus.h"

void UserStatus::setAverageAnswerTime(float averageAnswerTime)
{
	this->_averageAnswerTime = averageAnswerTime;
}

void UserStatus::setCorrectAnswers(int correctAnswers)
{
	this->_correctAnswers = correctAnswers;
}

void UserStatus::setTotalAnswers(int totalAnswers)
{
	this->_totalAnswers = totalAnswers;
}

void UserStatus::setPlayerGames(int playerGames)
{
	this->_playerGames = playerGames;
}


float UserStatus::getAverageAnswerTime()
{
	return this->_averageAnswerTime;
}

int UserStatus::getCorrectAnswers()
{
	return this->_correctAnswers;
}

int UserStatus::getTotalAnswers()
{
	return this->_totalAnswers;
}

int UserStatus::getPlayerGames()
{
	return this->_playerGames;
}

//do avg to all fileds in this class.
float UserStatus::getTotalStatistics()
{
	return ((this->_averageAnswerTime + this->_correctAnswers + this->_playerGames + this->_totalAnswers)/4);
}
