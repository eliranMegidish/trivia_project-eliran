#pragma once
#include <iostream>
using std::string;

class LoggedUser 
{
public:
	LoggedUser(string name);
	string getUserName();
	void setUserName(string name);

private:
	string m_username;

};