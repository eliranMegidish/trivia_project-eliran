#pragma once
#include <vector>
#include <string>
#include <WinSock2.h>
#include <bitset>
#include <sstream>
#include <iostream>
#include <map>
#include "json.hpp"

#define LEN_CODE_MSG 8

enum MessageType : byte
{
	// this code message that server send to client 
	CODE_SERVER_LOGIN = 101,
	CODE_SERVER_SIGNUP = 102,
	CODE_SERVER_ERROR = 103,
	CODE_SERVER_CREATE_ROOM = 104,
	CODE_SERVER_JOIN_ROOM = 105,
	CODE_SERVER_PLAYER_IN_ROOM = 106,
	CODE_SERVER_LOGOUT = 107,
	CODE_SERVER_GET_ROOMS = 108,
	CODE_SERVER_GET_PLAYERS_IN_ROOM = 109,
	CODE_SERVER_STATISTIC = 110,
	CODE_SERVER_HIGH_SCORES = 111,



	// this code message that client send to server 
	CODE_CLIENT_LOGIN = 1,
	CODE_CLIENT_SIGNUP = 2,
	CODE_CLIENT_LOGOUT = 4,
	CODE_CLIENT_CREATE_ROOM = 5,
	CODE_CLIENT_ROOM_LIST = 6,
	CODE_CLIENT_JOIN_ROOM = 7,
	CODE_CLIENT_PLAYER_IN_ROOM = 8,
	CODE_CLIENT_STATISTICS = 9,
	CODE_CLIENT_HIGH_SCORES = 10
};

class Helper
{
public:

	static std::vector<char unsigned> getVectorMsg(std::string s);//get code message.
	static std::string getMessageCode(std::string s);//get code message.
	static std::string getMessage(std::string buffer);//get code message.
	static void sendData(SOCKET sc, std::string message);
	static std::string create_response_msg_from_sever(const int codeMsg, const int lenMsg, const std::string msg);
	static std::string getPaddedNumber(int num, int digits);
	static std::string stringToBinary(std::string msg);
	static std::string binaryToString(std::string binaryString );
	static nlohmann::json bin_to_string_to_json(std::vector<char unsigned> binaryMsg );
	static std::vector<char unsigned>  serialize(std::string buffer , const int codeMsg);
	static std::string  vectorToString(std::vector<char unsigned> vec);

};

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
	os << "[";
	for (int i = 0; i < v.size(); ++i) 
	{
		os << v[i];
		if (i != v.size() - 1)
			os << ", ";
	}
	os << "]\n";
	return os;
}



#ifdef _DEBUG // vs add this define in debug mode
#include <stdio.h>
// Q: why do we need traces ?
// A: traces are a nice and easy way to detect bugs without even debugging
// or to understand what happened in case we miss the bug in the first time
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
// for convenient reasons we did the traces in stdout
// at general we would do this in the error stream like that
// #define TRACE(msg, ...) fprintf(stderr, msg "\n", __VA_ARGS__);

#else // we want nothing to be printed in release version
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
#define TRACE(msg, ...) // do nothing
#endif