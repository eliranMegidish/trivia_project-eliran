﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace Client
{
    /// <summary>
    /// Interaction logic for WindoesHighScores.xaml
    /// </summary>
    public partial class WindoesHighScores : Window
    {
        NetworkStream _clientStream;
        public WindoesHighScores(NetworkStream clientStream)
        {
            InitializeComponent();
            this._clientStream = clientStream;
            byte[] buffer = Helper.createHighScoresMsg();
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
            buffer = new byte[4096];
            int bytesRead = clientStream.Read(buffer, 0, 4096);
            Statistics highScores = JsonConvert.DeserializeObject<Statistics>(Helper.getResponse(System.Text.Encoding.UTF8.GetString(buffer)));

            if (highScores.names.Count() == 1)
            {
                name1.Text = highScores.names[0];
                score1.Text = highScores.scores[0].ToString();
            }
            else if (highScores.names.Count() == 2)
            {
                name1.Text = highScores.names[0];
                score1.Text = highScores.scores[0].ToString();
                name2.Text = highScores.names[1];
                score2.Text = highScores.scores[1].ToString();
            }
            else
            {
                name1.Text = highScores.names[0];
                score1.Text = highScores.scores[0].ToString();
                name2.Text = highScores.names[1];
                score2.Text = highScores.scores[1].ToString();
                name3.Text = highScores.names[2];
                score3.Text = highScores.scores[2].ToString();
            }
        }
        private void backButton(object sender, RoutedEventArgs e)
        {
            Welcome welcome = new Welcome(this._clientStream);
            welcome.Show();
            Close();
        }
    }
}
