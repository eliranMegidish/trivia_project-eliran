﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace Client
{
    /// <summary>
    /// Interaction logic for RoomList.xaml
    /// </summary>
    public partial class RoomList : Window
    {
        NetworkStream _clientStream;

        public RoomList(NetworkStream clientStream)
        {
            InitializeComponent();
            this._clientStream = clientStream;
            byte[] buffer = Helper.createListRoomsMsg();
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
            buffer = new byte[4096];
            int bytesRead = clientStream.Read(buffer, 0, 4096);
            Rooms rooms = JsonConvert.DeserializeObject<Rooms>(Helper.getResponse(System.Text.Encoding.UTF8.GetString(buffer)));
       


            int i = 80;
            if (rooms.rooms!=null) // no has room to show
            {
                foreach (string roomName in rooms.rooms)
                {
                    TextBlock txt = new TextBlock();
                    txt.Text = roomName;
                    txt.FontSize = 25;
                    txt.Foreground = new SolidColorBrush(Colors.Red);
                    txt.Margin = new Thickness(70, i, 0, 0);
                    mainPnl.Children.Add(txt);
                    txt.Focus();
                    i = 20;
                }
            }
        }
        private void backButton(object sender, RoutedEventArgs e)
        {
            Welcome welcome = new Welcome(this._clientStream);
            welcome.Show();
            Close();
        }
    }
}
