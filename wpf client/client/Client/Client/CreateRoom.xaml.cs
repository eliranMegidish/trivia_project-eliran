﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace Client
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    public partial class CreateRoom : Window
    {
        NetworkStream _clientStream;

        public CreateRoom(NetworkStream clientStream)
        {
            this._clientStream = clientStream;
            InitializeComponent();
        }
        private void button_Send(object sender, RoutedEventArgs e)
        {
            if (textBoxRoomName.Text.Length != 0 && TextBoxNumberOfPlayer.Text.Length != 0 && TextBoxNumberOfQuestions.Text.Length != 0 && TextBoxTimeForQuestions.Text.Length != 0)
            {
                byte[] buffer = Helper.createRoomMsg(textBoxRoomName.Text, int.Parse(TextBoxNumberOfPlayer.Text), int.Parse(TextBoxNumberOfQuestions.Text), int.Parse(TextBoxTimeForQuestions.Text));
                //MessageBox.Show(System.Text.Encoding.UTF8.GetString(buffer));
                _clientStream.Write(buffer, 0, buffer.Length);
                _clientStream.Flush();

                buffer = new byte[4096];
                int bytesRead = _clientStream.Read(buffer, 0, 4096);
                //MessageBox.Show(Helper.getResponse(System.Text.Encoding.UTF8.GetString(buffer)));

                User user = JsonConvert.DeserializeObject<User>(Helper.getResponse(System.Text.Encoding.UTF8.GetString(buffer)));

                if (user.status == 0)
                {
                    errormessage.Text = "Error";
                    errormessage.Focus();
                }
                else
                {
                    WelcomeRoomManager RoomManager = new WelcomeRoomManager(textBoxRoomName.Text, int.Parse(TextBoxNumberOfPlayer.Text), int.Parse(TextBoxNumberOfQuestions.Text), int.Parse(TextBoxTimeForQuestions.Text));
                    RoomManager.Show();
                    this.Close();
                }
            }
            else
            {
                errormessage.Text = "הכנס את כל השדות ";
                errormessage.Focus();
            }
        }
        private void backButton(object sender, RoutedEventArgs e)
        {
            Welcome welcome = new Welcome(this._clientStream);
            welcome.Show();
            Close();
        }
    }
}
