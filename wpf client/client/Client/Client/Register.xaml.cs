﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Client
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : Window
    {
        NetworkStream _clientStream;

        public Register(NetworkStream clientStream)
        {
            this._clientStream = clientStream;
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            byte[] buffer = Helper.createRegisterMsg(textBoxName.Text, passwordBox.Password,textBoxEmail.Text);
            _clientStream.Write(buffer, 0, buffer.Length);
            _clientStream.Flush();
            //receve from server
            buffer = new byte[4096];
            int bytesRead = _clientStream.Read(buffer, 0, 4096);
            User user = JsonConvert.DeserializeObject<User>(Helper.getResponse(System.Text.Encoding.UTF8.GetString(buffer)));
            //MessageBox.Show(Helper.getResponse(System.Text.Encoding.UTF8.GetString(buffer)));
            if (user.status == 0)
            {
                errormessage.Text = "error";
                errormessage.Focus();
            }
            else
            {
                Welcome menu = new Welcome(_clientStream);
                menu.Show();
                this.Close();
            }
        }
        private void backButton(object sender, RoutedEventArgs e)
        {
            Login welcome = new Login(this._clientStream);
            welcome.Show();
            Close();
        }
    }
}
