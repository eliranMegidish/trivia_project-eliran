﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace Client
{
    /// <summary>
    /// Interaction logic for Welcome.xaml
    /// </summary>
    public partial class Welcome : Window
    {
        NetworkStream _clientStream;
        public Welcome(NetworkStream clientStream)
        {
            InitializeComponent();
            this._clientStream = clientStream;
        }

        private void signOutButton(object sender, RoutedEventArgs e)
        {
            byte[] buffer = Helper.createLogoutMsg();
            _clientStream.Write(buffer, 0, buffer.Length);
            _clientStream.Flush();
            //MessageBox.Show(System.Text.Encoding.UTF8.GetString(buffer));

            buffer = new byte[4096];
            int bytesRead = _clientStream.Read(buffer, 0, 4096);
            //MessageBox.Show(Helper.getResponse(System.Text.Encoding.UTF8.GetString(buffer)));

            User user = JsonConvert.DeserializeObject<User>(Helper.getResponse(System.Text.Encoding.UTF8.GetString(buffer)));
            if (user.status == 0)
            {
                errormessage.Text = "can't Logout";
                errormessage.Focus();
            }
            else 
            {
                Login login = new Login(_clientStream);
                login.Show();
                Close();
            }
        }
        private void JoinRoomButton(object sender, RoutedEventArgs e)
        {
            RoomList rooms = new RoomList(_clientStream);
            rooms.Show();
            Close();
        }
        private void CreateRoomButton(object sender, RoutedEventArgs e)
        {
            CreateRoom createRoom = new CreateRoom(_clientStream);
            createRoom.Show();
            Close();
        }
        private void MyStatusButton(object sender, RoutedEventArgs e)
        {
            UserStatus status = new UserStatus(_clientStream);
            status.Show();
            Close();
        }
        private void BestScoresButton(object sender, RoutedEventArgs e)
        {
            WindoesHighScores highScores = new WindoesHighScores(_clientStream);
            highScores.Show();
            Close();
        }
    }
}
