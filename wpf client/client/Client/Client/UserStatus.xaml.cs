﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using Newtonsoft.Json;
namespace Client
{
    /// <summary>
    /// Interaction logic for UserStatus.xaml
    /// </summary>
    public partial class UserStatus : Window
    {
        NetworkStream _clientStream;
        public UserStatus(NetworkStream clientStream)
        {
            InitializeComponent();
            this._clientStream = clientStream;
            byte[] buffer = Helper.createGetStatisticsMsg();
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
            buffer = new byte[4096];
            int bytesRead = clientStream.Read(buffer, 0, 4096);
            Statistics statisticsUser = JsonConvert.DeserializeObject<Statistics>(Helper.getResponse(System.Text.Encoding.UTF8.GetString(buffer)));
            NumberGames.Text = statisticsUser.Player_Games.ToString();
            rightAnswers.Text = statisticsUser.Correct_Answers.ToString();
            TotalAns.Text = statisticsUser.Total_Answers.ToString();
            AvgtimeAnswers.Text = statisticsUser.Average_Answer_Time.ToString();

        }
        private void backButton(object sender, RoutedEventArgs e)
        {
            Welcome welcome = new Welcome(this._clientStream);
            welcome.Show();
            Close();
        }
    }
}
