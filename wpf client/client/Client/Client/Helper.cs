﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO;
public enum codeMsg
{
    CODE_CLIENT_LOGIN = 1,
    CODE_CLIENT_SIGNUP = 2,
    CODE_CLIENT_LOGOUT = 4,
    CODE_CLIENT_CREATE_ROOM = 5,
    CODE_CLIENT_ROOM_LIST = 6,
    CODE_CLIENT_JOIN_ROOM = 7,
    CODE_CLIENT_PLAYER_IN_ROOM = 8,
    CODE_CLIENT_STATISTIC = 9,
    CODE_CLIENT_HIGH_SCORES = 10

}

/*
   על פי הנתונים המתקבלים כפקמטרים בפונקציה ואז  ליצור קוד הודעה אורך ההודעה וההודעה עצמה jason המטרה היא לעדכן  
*/
namespace Client
{
    class Helper
    {

        public static Byte[] BinaryToString(string data)
        {
            byte[] result = new byte[(data.Length + 7) / 8];

            int i = 0;
            int j = 0;
            foreach (char c in data)
            {
                result[i] <<= 1;
                if (c == '1')
                    result[i] |= 1;
                j++;
                if (j == 8)
                {
                    i++;
                    j = 0;
                }
            }
            return result;
        }

        public static string getResponse(string buffer)
        {
            buffer = buffer.Remove(0, 40);
            return Encoding.ASCII.GetString(BinaryToString(buffer));
        }
        public static string stringToBinary(string data, bool formatBits = false)
        {
            char[] buffer = new char[(((data.Length * 8) + (formatBits ? (data.Length - 1) : 0)))];
            int index = 0;
            for (int i = 0; i < data.Length; i++)
            {
                string binary = Convert.ToString(data[i], 2).PadLeft(8, '0');
                for (int j = 0; j < 8; j++)
                {
                    buffer[index] = binary[j];
                    index++;
                }
                if (formatBits && i < (data.Length - 1))
                {
                    buffer[index] = ' ';
                    index++;
                }
            }
            return new string(buffer);
        }

        public static byte[] createLoginMsg(string userName, string password)
        {
            JObject jsonLogin = new JObject
            {
                ["username"] = userName,
                ["password"] = password
            };
            string fullMsg = (Convert.ToString((int)codeMsg.CODE_CLIENT_LOGIN, 2)).PadLeft(8, '0') + Convert.ToString((stringToBinary(jsonLogin.ToString())).Length, 2).PadLeft(32, '0') + stringToBinary(jsonLogin.ToString());
            //byte[] bytes = Encoding.ASCII.GetBytes(fullMsg);
            // return bytes;
            return Encoding.ASCII.GetBytes(fullMsg);
        }

        public static byte[] createLogoutMsg()
        {
            string fullMsg = (Convert.ToString((int)codeMsg.CODE_CLIENT_LOGOUT, 2).PadLeft(8, '0') + "0".PadLeft(32, '0'));
            return Encoding.ASCII.GetBytes(fullMsg);
        }
        public static byte[] createRegisterMsg(string userName, string password,string email)
        {
            JObject jsonLogin = new JObject
            {
                ["username"] = userName,
                ["password"] = password,
                ["email"] = email
            };
            string fullMsg = (Convert.ToString((int)codeMsg.CODE_CLIENT_SIGNUP, 2)).PadLeft(8, '0') + Convert.ToString((stringToBinary(jsonLogin.ToString())).Length, 2).PadLeft(32, '0') + stringToBinary(jsonLogin.ToString());
            return Encoding.ASCII.GetBytes(fullMsg);
        }

        public static byte[] createRoomMsg(string roomName,int numberOfPlayer,int numberOfQuestions,int TimeForQuestions)
        {
            JObject jsonLogin = new JObject
            {
                ["roomName"] = roomName,
                ["maxUsers"] = numberOfPlayer,
                ["questionCount"] = numberOfQuestions,
                ["answerTimeout"] = TimeForQuestions
            };
            string fullMsg = (Convert.ToString((int)codeMsg.CODE_CLIENT_CREATE_ROOM, 2)).PadLeft(8, '0') + Convert.ToString((stringToBinary(jsonLogin.ToString())).Length, 2).PadLeft(32, '0') + stringToBinary(jsonLogin.ToString());
            return Encoding.ASCII.GetBytes(fullMsg);
        }

        public static byte[] createListRoomsMsg()
        {
            string fullMsg = (Convert.ToString((int)codeMsg.CODE_CLIENT_ROOM_LIST, 2).PadLeft(8, '0') + "0".PadLeft(32, '0'));
            return Encoding.ASCII.GetBytes(fullMsg);
        }

        public static byte[] createGetStatisticsMsg()
        {
            string fullMsg = (Convert.ToString((int)codeMsg.CODE_CLIENT_STATISTIC, 2).PadLeft(8, '0') + "0".PadLeft(32, '0'));
            return Encoding.ASCII.GetBytes(fullMsg);
        }

        public static byte[] createHighScoresMsg()
        {
            string fullMsg = (Convert.ToString((int)codeMsg.CODE_CLIENT_HIGH_SCORES, 2).PadLeft(8, '0') + "0".PadLeft(32, '0'));
            return Encoding.ASCII.GetBytes(fullMsg);
        }
    }
}

