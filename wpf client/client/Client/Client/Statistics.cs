﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Statistics
    {
        public float Average_Answer_Time;
        public int Correct_Answers;
        public int Player_Games;
        public int Total_Answers;
        public new List<string> names;
        public new List<float> scores;

    }
}
