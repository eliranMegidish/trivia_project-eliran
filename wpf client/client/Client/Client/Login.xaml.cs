﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace Client
{   
    public partial class Login : Window
    {
        NetworkStream _clientStream;
        public Login(NetworkStream clientStream)
        {
           
            InitializeComponent();
            this._clientStream = clientStream;
        }
        

        private void LoginButton(object sender, RoutedEventArgs e)
        {
            if (textBoxName.Text.Length != 0 && passwordBox.Password.Length != 0 )
            {
                byte[] buffer = Helper.createLoginMsg(textBoxName.Text, passwordBox.Password);
                 _clientStream.Write(buffer, 0, buffer.Length);
                 _clientStream.Flush();

                //receve from server
                buffer = new byte[4096];
                int bytesRead = _clientStream.Read(buffer, 0, 4096);
                User user = JsonConvert.DeserializeObject<User>( Helper.getResponse(System.Text.Encoding.UTF8.GetString(buffer)));
                //MessageBox.Show(Helper.getResponse(System.Text.Encoding.UTF8.GetString(buffer)));
                if (user.status == 0)
                {
                    errormessage.Text = "Error try agin";
                    errormessage.Focus();     
                }
                else
                {
                    Welcome menu = new Welcome(_clientStream);
                    menu.Show();
                    this.Close(); 
                }
            }
            else
            {
                errormessage.Text = "enter email or password";
                errormessage.Focus();
            }
        }
        private void RegisterButton(object sender, RoutedEventArgs e)
        {
            Register register = new Register(_clientStream);
            register.Show();
            this.Close();
        }
    }
}
