﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


public enum codeMsg
{
    CODE_CLIENT_LOGIN = 1,
    CODE_CLIENT_SIGNUP = 2,
    CODE_CLIENT_LOGOUT = 4,
    CODE_CLIENT_CREATE_ROOM = 5,
    CODE_CLIENT_JOIN_ROOM = 6,
    CODE_CLIENT_PLAYER_IN_ROOM = 7
}

/*
   על פי הנתונים המתקבלים כפקמטרים בפונקציה ואז  ליצור קוד הודעה אורך ההודעה וההודעה עצמה jason המטרה היא לעדכן  
*/
namespace Client
{
    class Helper
    {
        public static string convertStringToBinary(string str)
        {

            string jsonInBinary = string.Empty;
            foreach (char ch in str)
            {
                jsonInBinary += Convert.ToString((int)ch, 2);
            }
            return jsonInBinary;
        }

        public static byte[] createLoginMsg(string userName, string password)
        {
            JObject jsonLogin = new JObject
            {
                ["username"] = userName,
                ["password"] = password
            };
            string fullMsg = (Convert.ToString((int)codeMsg.CODE_CLIENT_LOGIN, 2)).PadLeft(8, '0') + Convert.ToString((convertStringToBinary(jsonLogin.ToString())).Length, 2).PadLeft(32, '0') + convertStringToBinary(jsonLogin.ToString());
            MessageBox.Show(Convert.ToString((convertStringToBinary(jsonLogin.ToString())).Length));
            MessageBox.Show((Convert.ToString((int)codeMsg.CODE_CLIENT_LOGIN, 2)).PadLeft(8, '0'));
            MessageBox.Show(Convert.ToString((convertStringToBinary(jsonLogin.ToString())).Length, 2));
            MessageBox.Show(convertStringToBinary(jsonLogin.ToString()));
            byte[] bytes = Encoding.ASCII.GetBytes(fullMsg);
            return bytes;
        }


    }
}

