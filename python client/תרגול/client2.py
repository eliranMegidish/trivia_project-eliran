import socket
SERVER_IP = "127.0.0.1"
SERVER_PORT = 8826

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = (SERVER_IP, SERVER_PORT)
sock.connect(server_address)

msg=""
while not(msg =="EXIT"):
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    print(server_msg)
    msg = input("Enter msg::")
    sock.sendall(msg.encode())
